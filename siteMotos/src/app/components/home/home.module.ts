import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DestaquesComponent } from './destaques/destaques.component';
import { NavbarComponent } from './navbar/navbar.component';
import { CardMotoComponent } from './card-moto/card-moto.component';
import { CardServicoComponent } from './card-servico/card-servico.component';
import { CompreComponent } from './compre/compre.component';
import { FooterComponent } from './footer/footer.component';



@NgModule({
  declarations: [
    DestaquesComponent,
    NavbarComponent,
    CardMotoComponent,
    CardServicoComponent,
    CompreComponent,
    FooterComponent,
  ],
  imports: [
    CommonModule,
  ]
})
export class HomeModule { }
