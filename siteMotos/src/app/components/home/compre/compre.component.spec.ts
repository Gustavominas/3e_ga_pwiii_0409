import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompreComponent } from './compre.component';

describe('CompreComponent', () => {
  let component: CompreComponent;
  let fixture: ComponentFixture<CompreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
