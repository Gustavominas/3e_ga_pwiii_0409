import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './components/home/home.component';
import { TrailComponent } from './components/trail/trail.component';
import { StreetComponent } from './components/street/street.component';
import { NavbarComponent } from './components/home/navbar/navbar.component';
import { DestaquesComponent } from './components/home/destaques/destaques.component';
import { CardMotoComponent } from './components/home/card-moto/card-moto.component';
import { CardServicoComponent } from './components/home/card-servico/card-servico.component';
import { CompreComponent } from './components/home/compre/compre.component';
import { FooterComponent } from './components/home/footer/footer.component';
import { CustomComponent } from './components/custom/custom.component';
import { ScooterComponent } from './components/scooter/scooter.component';
import { SportComponent } from './components/sport/sport.component';
import { TouringComponent } from './components/touring/touring.component';
import { LoginComponent } from './components/login/login.component';
import { CadastroUsuariosComponent } from './components/cadastro-usuarios/cadastro-usuarios.component';
import { CadastroClientesComponent } from './components/cadastro-clientes/cadastro-clientes.component';
import { CadastroMotosComponent } from './components/cadastro-motos/cadastro-motos.component';
import { AcompanhamentoPedidosComponent } from './components/acompanhamento-pedidos/acompanhamento-pedidos.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TrailComponent,
    StreetComponent,
    NavbarComponent,
    DestaquesComponent,
    CardMotoComponent,
    CardServicoComponent,
    CompreComponent,
    FooterComponent,
    CustomComponent,
    ScooterComponent,
    SportComponent,
    TouringComponent,
    LoginComponent,
    CadastroUsuariosComponent,
    CadastroClientesComponent,
    CadastroMotosComponent,
    AcompanhamentoPedidosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
